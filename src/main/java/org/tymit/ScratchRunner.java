package org.tymit;

import org.tymit.projectdonut.model.location.TransStation;
import org.tymit.projectdonut.stations.gtfs.GtfsProvider;
import org.tymit.projectdonut.stations.postgresql.PostgresSqlSupport;
import org.tymit.projectdonut.stations.postgresql.PostgresqlStationDbCache_LoadVersion;
import org.tymit.projectdonut.utils.LoggingUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ilan on 1/5/17.
 */
public class ScratchRunner {



    public static final String[][] credentialList = new String[][] {
            { "AKIAISDSP6RHQG2RARNA", "HByI/CwMsL8fobViNGe63Lob0jkpIXLA7iiEAwiE" }
    };

    private static final String OWNER_TAG = initOwnerTag();
    private static final LoadRequestManager manager = new LoadRequestManager(OWNER_TAG, credentialList[0]);
    private static final PostgresqlStationDbCache_LoadVersion db =
            new PostgresqlStationDbCache_LoadVersion("jdbc:postgresql://192.168.1.71:5432/Donut");

    public static void main(String[] args) throws Exception {

        if (Arrays.stream(args).anyMatch(a -> a.equals("-lv"))) {
            mainLoadVersion();
            return;
        }

        LoggingUtils.setPrintImmediate(true);

        manager.getCandidateRequests()
                .peek(a -> LoggingUtils.logMessage(OWNER_TAG, "Now processing url %s.", a.toString()))
                .map(GtfsProvider::new)
                .forEach((LoggingUtils.WrappedConsumer<GtfsProvider>) prov -> {
                    String fileName = "/home/ilan/Projects/aaa__" + prov.getSource().replace("/", "_") + ".psql";
                    Files.deleteIfExists(Paths.get(fileName));
                    Files.createFile(Paths.get(fileName));
                    List<TransStation> allStations = new ArrayList<>();
                    for (List<TransStation> stationList : prov.getUpdatedStations().values()) {
                        allStations.addAll(stationList);
                    }
                    PostgresSqlSupport.createInsertStatements(allStations)
                            .forEach((LoggingUtils.WrappedConsumer<String>) s -> Files.write(Paths.get(fileName), s.getBytes(), StandardOpenOption.APPEND));

                });


    }

    public static void mainLoadVersion() throws Exception {

        LoggingUtils.setPrintImmediate(true);

        manager.getAllUrls()
                .parallel()
                .peek(a -> LoggingUtils.logMessage(OWNER_TAG, "Now processing url %s in load mode.", a.toString()))
                .map(GtfsProvider::new)
                .forEach((LoggingUtils.WrappedConsumer<GtfsProvider>) prov -> {
                    String fileName = "/home/ilan/Projects/aaa__" + prov.getSource().replace("/", "_") + ".psql";
                    Files.deleteIfExists(Paths.get(fileName));
                    Files.createFile(Paths.get(fileName));
                    List<TransStation> allStations = new ArrayList<>();
                    for (List<TransStation> stationList : prov.getUpdatedStations().values()) {
                        allStations.addAll(stationList);
                    }
                    PostgresSqlSupport.createInsertStatements(allStations)
                            .forEach((LoggingUtils.WrappedConsumer<String>) s -> Files.write(Paths.get(fileName), s.getBytes(), StandardOpenOption.APPEND));

                });


    }

    private static String initOwnerTag() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            return System.getenv("HOSTNAME");
        }
    }


}

