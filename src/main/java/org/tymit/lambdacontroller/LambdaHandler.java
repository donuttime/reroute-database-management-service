package org.tymit.lambdacontroller;


import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import org.tymit.LoadRequestManager;

/**
 * Created by ilan on 6/4/17.
 */
public class LambdaHandler implements RequestHandler<DynamodbEvent, Object>{

    public static final String[][] credentialList = new String[][] {
            { "AKIAISDSP6RHQG2RARNA", "HByI/CwMsL8fobViNGe63Lob0jkpIXLA7iiEAwiE" }
    };

    private static final String OWNER_TAG = "Lambda";

    private static final LoadRequestManager manager = new LoadRequestManager(OWNER_TAG, credentialList[0]);

    @Override
    public Object handleRequest(DynamodbEvent event, Context context) {
        manager.getCandidateRequests()
                .findAny()
                .ifPresent(manager::processUrl);
        return null;
    }

}
