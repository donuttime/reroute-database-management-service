package org.tymit.projectdonut.stations.postgresql;

import org.tymit.projectdonut.model.location.TransChain;
import org.tymit.projectdonut.model.location.TransStation;
import org.tymit.projectdonut.model.time.TimeDelta;
import org.tymit.projectdonut.model.time.TimePoint;
import org.tymit.projectdonut.stations.interfaces.StationCacheInstance;
import org.tymit.projectdonut.stations.interfaces.StationDbInstance;
import org.tymit.projectdonut.utils.LoggingUtils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

/**
 * Created by ilan on 3/31/17.
 */
public class PostgresqlStationDbCache_LoadVersion implements StationCacheInstance, StationDbInstance.ComboDb {

    public static final String[] DB_URLS = new String[] { "jdbc:postgresql://donutdb.c3ovzbdvtevz.us-west-2.rds.amazonaws.com:5432/Donut" };
    private static final String USER = "donut";
    private static final String PASS = "donutpass";
    private final Connection con;
    private String url;
    private boolean isUp;

    public PostgresqlStationDbCache_LoadVersion(String url) {

        isUp = true;
        this.url = url;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (Exception e) {
            LoggingUtils.logError(e);
            isUp = false;
        }

        Connection tempcon;
        try {
            tempcon = DriverManager.getConnection(url, USER, PASS);
            tempcon.setAutoCommit(false);
        } catch (SQLException e) {
            LoggingUtils.logError(e);
            isUp = false;
            tempcon = null;
        }
        con = tempcon;
    }

    private Connection getConnection() {
        return con;
    }

    @Override
    public boolean putStations(List<TransStation> stations) {
        if (!isUp) return false;
        try {
            return PostgresSqlSupport_LoadVersion.storeStations(this::getConnection, stations);
        } catch (SQLException e) {
            LoggingUtils.logError(e);
            isUp = false;
            return false;
        }
    }

    public boolean isUp() {
        return isUp;
    }

    public void close() {
        isUp = false;
        try {
            con.close();
        } catch (Exception e) {
            LoggingUtils.logError(e);
        }
    }

    @Override
    public boolean cacheStations(double[] center, double range, TimePoint startTime, TimeDelta maxDelta, List<TransStation> stations) {
        return false;
    }

    @Override
    public List<TransStation> getCachedStations(double[] center, double range, TimePoint startTime, TimeDelta maxDelta, TransChain chain) {
        return Collections.emptyList();
    }

    @Override
    public List<TransStation> queryStations(double[] center, double range, TimePoint startTime, TimeDelta maxDelta, TransChain chain) {
        return Collections.emptyList();
    }
}
