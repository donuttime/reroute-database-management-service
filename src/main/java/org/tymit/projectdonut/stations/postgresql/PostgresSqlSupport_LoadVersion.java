package org.tymit.projectdonut.stations.postgresql;

import org.tymit.projectdonut.model.location.TransChain;
import org.tymit.projectdonut.model.location.TransStation;
import org.tymit.projectdonut.utils.LoggingUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Created by ilan on 3/28/17.
 */
public class PostgresSqlSupport_LoadVersion {


    private static final String LAT_COL_NAME = "statlatman";
    private static final String LNG_COL_NAME = "statlngman";
    private static final String HOUR_COL_NAME = "hourman";
    private static final String MINUTE_COL_NAME = "minuteman";
    private static final String SECOND_COL_NAME = "secondman";
    private static final String STAT_NAM_KEY = "statnm";
    private static final String CHN_NAM_KEY = "chanm";


    private static final long MAX_POSTGRES_INTERVAL_MILLI = 1000L * 60L * 60L * 24L * 365L * 10L;
    private static final long BATCH_SIZE = 300;
    private static final long ERROR_MARGIN = 1; //meters


    /**
     * INFORMATION STORING
     **/

    public static boolean storeStations(Supplier<Connection> conGenerator, Collection<? extends TransStation> stations
    ) throws SQLException {
        Connection con = conGenerator.get();
        Statement stm = con.createStatement();

        //Insert the chains into the database in a batch
        AtomicInteger ctprev = new AtomicInteger(0);
        stations.stream()
                .map(TransStation::getChain)
                .filter(Objects::nonNull)
                .distinct()
                .map(TransChain::getName)
                .map(chainName -> String.format("INSERT INTO %s(%s) VALUES (\'%s\');",
                        PostgresqlContract_LoadVersion.ChainTable.TABLE_NAME, PostgresqlContract_LoadVersion.ChainTable.NAME_KEY,
                        chainName.replace("'", "`"))
                )
                .filter(Objects::nonNull)
                .forEach((LoggingUtils.WrappedConsumer<String>) (sql1) -> {
                    stm.addBatch(sql1);
                    if (ctprev.incrementAndGet() >= BATCH_SIZE) {
                        ctprev.getAndSet(0);
                        stm.executeBatch();
                    }
                });
        if (ctprev.get() != 0) stm.executeBatch();

        //Then the stations using a batch
        ctprev.set(0);
        stations.stream()
                .map(TransStation::stripSchedule)
                .distinct()
                .map(station -> String.format("INSERT INTO %s(%s, %s) VALUES (\'%s\', ST_POINT(%f,%f)::geography);",
                        PostgresqlContract_LoadVersion.StationTable.TABLE_NAME, PostgresqlContract_LoadVersion.StationTable.NAME_KEY, PostgresqlContract_LoadVersion.StationTable.LATLNG_KEY,
                        station.getName()
                                .replace("'", "`"), station.getCoordinates()[0], station
                                .getCoordinates()[1])
                )
                .forEach((LoggingUtils.WrappedConsumer<String>) (sql1) -> {
                    stm.addBatch(sql1);
                    if (ctprev.incrementAndGet() >= BATCH_SIZE) {
                        ctprev.getAndSet(0);
                        stm.executeBatch();
                    }
                });

        //Insert schedule information
        ctprev.set(0);
        stations.stream()
                .flatMap(PostgresSqlSupport_LoadVersion::createScheduleQuery)
                .forEach((LoggingUtils.WrappedConsumer<String>) (sql) -> {
                    stm.addBatch(sql);
                    if (ctprev.incrementAndGet() >= BATCH_SIZE) {
                        ctprev.getAndSet(0);
                        stm.executeBatch();
                    }
                });
        if (ctprev.get() != 0) stm.executeBatch();

        stm.close();
        return true;
    }

    public static Stream<String> createInsertStatements(Collection<? extends TransStation> stations) {

        //Insert the chains into the database in a batch
        Stream<String> chainInsert = stations.stream()
                .map(TransStation::getChain)
                .filter(Objects::nonNull)
                .distinct()
                .map(TransChain::getName)
                .map(chainName -> String.format("INSERT INTO %s(%s) VALUES (\'%s\');\n",
                        PostgresqlContract_LoadVersion.ChainTable.TABLE_NAME, PostgresqlContract_LoadVersion.ChainTable.NAME_KEY, chainName
                                .replace("'", "`"))
                )
                .filter(Objects::nonNull);

        //Then the stations using a batch
        Stream<String> stationInsert = stations.stream()
                .map(TransStation::stripSchedule)
                .distinct()
                .map(station -> String.format("INSERT INTO %s(%s, %s) VALUES (\'%s\', ST_POINT(%f,%f)::geography);\n",
                        PostgresqlContract_LoadVersion.StationTable.TABLE_NAME, PostgresqlContract_LoadVersion.StationTable.NAME_KEY, PostgresqlContract_LoadVersion.StationTable.LATLNG_KEY,
                        station.getName()
                                .replace("'", "`"), station.getCoordinates()[0], station
                                .getCoordinates()[1])
                );

        //Insert schedule information
        Stream<String> scheduleInsert = stations.stream()
                .flatMap(PostgresSqlSupport_LoadVersion::createScheduleQuery);

        return Stream.concat(Stream.concat(stationInsert, chainInsert), scheduleInsert);

    }

    private static Stream<String> createScheduleQuery(TransStation station) {
        String costStatement = String.format(
                "INSERT INTO %s(%s, %s) VALUES (\'%s\', ST_POINT(%f, %f)::geography);\n",
                PostgresqlContract_LoadVersion.CostTable.STATION_CHAIN_COST_TABLE_NAME, PostgresqlContract_LoadVersion.CostTable.COST_CHAIN_KEY, PostgresqlContract_LoadVersion.CostTable.COST_STATION_KEY,
                station.getChain().getName().replace("'", "`"), station.getCoordinates()[0], station.getCoordinates()[1]
        );

        return Stream.concat(
                Stream.of(costStatement),
                station.getSchedule().stream().map(sched -> String.format(
                        "INSERT INTO %s(%s, %s, %s, %s, %s)\n" +
                                "VALUES (B'%s', '%d:%d:%d', %d, \'%s\', ST_POINT(%f, %f)::geography);\n",
                        PostgresqlContract_LoadVersion.ScheduleTable.TABLE_NAME,
                        PostgresqlContract_LoadVersion.ScheduleTable.PACKED_VALID_KEY,
                        PostgresqlContract_LoadVersion.ScheduleTable.TIME_KEY,
                        PostgresqlContract_LoadVersion.ScheduleTable.FUZZ_KEY,
                        PostgresqlContract_LoadVersion.ScheduleTable.COST_CHAIN_KEY,
                        PostgresqlContract_LoadVersion.ScheduleTable.COST_STATION_KEY,
                        booleanArrToBitString(sched.getValidDays()), sched.getHour(), sched.getMinute(), sched.getSecond(),
                        sched.getFuzz(), station.getChain().getName().replace("'", "`"),
                        station.getCoordinates()[0], station.getCoordinates()[1]
                )));
    }

    private static String booleanArrToBitString(boolean[] bols) {
        StringBuilder builder = new StringBuilder();
        for (boolean bol : bols) {
            builder.append(bol ? '1' : '0');
        }
        return builder.toString();
    }
}
