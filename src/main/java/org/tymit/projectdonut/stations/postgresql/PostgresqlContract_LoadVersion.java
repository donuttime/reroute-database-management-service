package org.tymit.projectdonut.stations.postgresql;

/**
 * Created by ilan on 3/7/17.
 */
public final class PostgresqlContract_LoadVersion {

    private PostgresqlContract_LoadVersion() {
    }

    public static final class ChainTable {

        public static final String TABLE_NAME = "transchain";
        public static final String NAME_KEY = "name";
        public static final String CREATE_TABLE =
                "CREATE TABLE public.transchain" +
                        "(" +
                        "  name text" +
                        ")" +
                        "WITH (" +
                        "  OIDS=FALSE" +
                        ");";


        private ChainTable() {
        }
    }

    public static final class StationTable {

        public static final String TABLE_NAME = "transstation";
        public static final String NAME_KEY = "name";
        public static final String LATLNG_KEY = "latlng";

        private StationTable() {
        }
    }

    public static final class CostTable {

        public static final String STATION_CHAIN_COST_TABLE_NAME = "StationChainCosts";
        public static final String COST_STATION_KEY = "stationlocation";
        public static final String COST_CHAIN_KEY = "chainname";

        private CostTable() {
        }

    }

    public static final class ScheduleTable {

        public static final String TABLE_NAME = "schedule";
        public static final String FUZZ_KEY = "fuzz";
        public static final String TIME_KEY = "schedpoint";
        public static final String PACKED_VALID_KEY = "validdays";
        public static final String COST_STATION_KEY = "stationlocation";
        public static final String COST_CHAIN_KEY = "chainname";

        private ScheduleTable() {
        }
    }

}
