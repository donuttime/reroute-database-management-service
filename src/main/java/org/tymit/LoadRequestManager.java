package org.tymit;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.AttributeUpdate;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import org.tymit.projectdonut.model.location.TransChain;
import org.tymit.projectdonut.model.location.TransStation;
import org.tymit.projectdonut.stations.gtfs.GtfsProvider;
import org.tymit.projectdonut.stations.postgresql.PostgresqlStationDbCache;
import org.tymit.projectdonut.utils.LoggingUtils;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Created by ilan on 7/13/17.
 */
public class LoadRequestManager {

    public static final String[][] credentialList = new String[][] {
            { "AKIAISDSP6RHQG2RARNA", "HByI/CwMsL8fobViNGe63Lob0jkpIXLA7iiEAwiE" }
    };

    private static final String TABLE_NAME = "ZipStoreRequests";
    private static final long DELAY_BEFORE_RETRY = 3600000;
    private static final int BULK_SIZE = 1000;

    private final DynamoDB db;
    private final PostgresqlStationDbCache tdb;
    private final String tag;

    public LoadRequestManager(String tag, String[] credentials) {

        this.tag = tag;
        AWSCredentials credentials1 = new BasicAWSCredentials(credentials[0], credentials[1]);
        AWSCredentialsProvider provider = new AWSStaticCredentialsProvider(credentials1);
        AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
                .withCredentials(provider)
                .withRegion(Regions.US_WEST_2)
                .build();

        db = new DynamoDB(client);

        tdb = new PostgresqlStationDbCache(PostgresqlStationDbCache.DB_URLS[0]);
    }

    public Stream<URL> getCandidateRequests() {
        Table tbl = db.getTable(TABLE_NAME);
        ScanSpec spec = new ScanSpec()
                .withAttributesToGet("Url", "Owner", "Started", "Finished")
                .withMaxResultSize(100);
        ItemCollection<ScanOutcome> result = tbl.scan(spec);
        return StreamSupport.stream(result.spliterator(), false)
                .filter(this::canUseItem)
                .map(item -> item.getString("Url"))
                .map((LoggingUtils.WrappedFunction<String, URL>) URL::new);

    }

    public Stream<URL> getAllUrls() {
        Table tbl = db.getTable(TABLE_NAME);
        ScanSpec spec = new ScanSpec()
                .withAttributesToGet("Url", "Owner", "Started", "Finished")
                .withMaxResultSize(100);
        ItemCollection<ScanOutcome> result = tbl.scan(spec);
        return StreamSupport.stream(result.spliterator(), false)
                .map(item -> item.getString("Url"))
                .map((LoggingUtils.WrappedFunction<String, URL>) URL::new);

    }

    public Stream<Map<String, Object>> getAllRequestInfo() {
        Table tbl = db.getTable(TABLE_NAME);
        ScanSpec spec = new ScanSpec()
                .withAttributesToGet("Url", "Owner", "Started", "Finished")
                .withMaxResultSize(100);
        ItemCollection<ScanOutcome> result = tbl.scan(spec);
        return StreamSupport.stream(result.spliterator(), false)
                .map(Item::asMap);

    }

    public boolean processUrl(URL url) {
        if (!isValid(url)) {
            LoggingUtils.logError(tag, "Invalid url: %s.", url);
            return false;
        }
        if (!tdb.isUp()) {
            LoggingUtils.logError(tag, "Database connection is not up.");
            return false;
        }
        setStarted(url);
        GtfsProvider prov = new GtfsProvider(url);
        Map<TransChain, List<TransStation>> toput = prov.getUpdatedStations();
        int numchains = toput.size();
        int i = 0;
        boolean success = true;
        List<TransStation> bulk = new ArrayList<>(BULK_SIZE);
        for (List<TransStation> stats : toput.values()) {
            bulk.addAll(stats);
            i++;
            if (bulk.size() >= BULK_SIZE) {
                success &= tdb.putStations(bulk);
                bulk.clear();
                LoggingUtils.logMessage(url.toString(), i + "/" + numchains);
                if (!tdb.isUp()) {
                    LoggingUtils.logError(tag, "Database connection is broken.");
                    return false;
                }
            }
        }
        if (success) setFinished(url);
        else setError(url);
        return success;
    }

    private void setFinished(URL url) {
        db.getTable(TABLE_NAME)
                .updateItem("Url", url.toString(), new AttributeUpdate("Finished").put(System.currentTimeMillis()));
    }

    private void setError(URL url) {
        AttributeUpdate at = new AttributeUpdate("Error").put(System.currentTimeMillis());
        db.getTable(TABLE_NAME).updateItem(new PrimaryKey("Url", url.toString()), at);
    }

    private void setStarted(URL url) {
        LoggingUtils.logMessage(tag, "Starting url %s.", url.toString());
        db.getTable(TABLE_NAME)
                .updateItem("Url", url.toString(), new AttributeUpdate("Started").put(System.currentTimeMillis()), new AttributeUpdate("Owner")
                        .put(tag));
    }

    public boolean isValid(URL url) {
        Item urlItem = db.getTable(TABLE_NAME).getItem("Url", url.toString());
        return canUseItem(urlItem);
    }

    private boolean canUseItem(Item item) {
        if (item.hasAttribute("Finished")) return false; //Already finished; no need to do again.
        if (!item.hasAttribute("Owner")) return true; //Unfinished and unclaimed; its ours now.
        if (!item.getString("Owner").equals(tag)) return false; //Someone else is already on it.
        return System.currentTimeMillis() - item.getLong("Started") > DELAY_BEFORE_RETRY; //We are not already working on it. Try again!
    }

}
